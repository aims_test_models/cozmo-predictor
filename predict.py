import base64
import logging

import numpy as np
from PIL import Image
from keras.applications.xception import Xception, preprocess_input, decode_predictions
from keras.preprocessing import image
import json

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Predictor:
    model = None

    def load(self):
        logger.info("loading model...")
        self.model = Xception(weights='./xception_weights_tf_dim_ordering_tf_kernels.h5')
        logger.info("loading model complete")


    def predict(self, raw_data_sample):
        logger.info("received image")
        logger.debug("decoding bytes")
        message = json.loads(raw_data_sample)
        logger.debug("decoding image")
        dec_image = self.decode_image(message['image_data'], message['image_size'])
        logger.debug("preprocessing image")
        dec_image = self.preprocess_image(dec_image)
        logger.debug("running inference")
        prediction = self.model.predict(dec_image)
        decoded_prediction = decode_predictions(prediction, 1)[0][0]
        logger.info("prediction: " + str(decoded_prediction))
        result = {"prediction": decoded_prediction[1],
                  "id": message["id"]}
        return json.dumps(result)

    def preprocess_image(self, dec_image):
        dec_image = dec_image.resize((224, 224), Image.LANCZOS)
        dec_image = image.img_to_array(dec_image)
        dec_image = np.expand_dims(dec_image, 0)
        dec_image = preprocess_input(dec_image)
        return dec_image

    def decode_image(self, base_64_image_data, image_size):
        image_data = base64.b64decode(base_64_image_data)
        dec_image = Image.frombytes(data=image_data, mode='RGB',
                                    size=image_size)
        return dec_image


def main():
    predictor = Predictor()
    predictor.load()

    img = image.load_img("test_image3.jpg")
    img_enc = base64.b64encode(img.tobytes()).decode('utf-8')
    message = {'image_data': img_enc,
               'image_size': img.size,
               'id': "asd"}
    result = predictor.predict(encode_message(message))
    print(result)

    with open("sample_data.json") as f:
        sample_file = json.load(f)
        sample_file["image_size"] = (320, 240)
        sample_file["id"] = "asdf"
        result = predictor.predict(encode_message(sample_file))
        print(result)


def encode_message(msg):
    return json.dumps(msg)


if __name__ == "__main__":
    main()
